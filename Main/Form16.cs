﻿using Global;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Main
{
    public partial class Form16 : Form
    {
        System.Media.SoundPlayer soundPlayer = new System.Media.SoundPlayer(@"C:\Users\Olexandr\Desktop\Bankomat\zvuk11.wav");

        public Form16()
        {
            InitializeComponent();
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            List<string> matchingPayments = SearchPaymentsByPhoneNumber(Globals.Number);
            dataGridView1.Rows.Clear(); // Очищення таблиці

            foreach (string payment in matchingPayments)
            {
                string[] paymentInfo = payment.Split('-');
                dataGridView1.Rows.Add(paymentInfo[0].Trim(), paymentInfo[1].Trim()); // Додавання рядка платежу до таблиці
            }
            timer1.Tick += Timer_Tick;
        }
        static List<string> SearchPaymentsByPhoneNumber(string Number)
        {
            List<string> matchingPayments = new List<string>();

            string filePath = "C:\\Users\\Olexandr\\Desktop\\Bankomat\\Number - TakeMoney.txt"; // Шлях до файлу блокноту з даними платежів
            string[] paymentData = File.ReadAllLines(filePath);

            foreach (string paymentLine in paymentData)
            {
                if (paymentLine.Contains(Number))
                {
                    matchingPayments.Add(paymentLine);
                }
            }

            return matchingPayments;
        }
        private void Timer_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            Form1 form1 = new Form1();
            form1.Show();

            this.Close();
        }
        private void Form16_Load(object sender, EventArgs e)
        {
            foreach (DataGridViewColumn column in dataGridView1.Columns)
            {
                column.AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            }
            foreach (DataGridViewColumn column in dataGridView1.Columns)
            {
                column.ReadOnly = true;
            }
            timer1.Start();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();


        }

        private void button7_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }

        private void button6_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }

        private void button5_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }
    }
}
