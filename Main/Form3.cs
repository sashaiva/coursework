﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;
using Global;

namespace Main
{
   

    public partial class Form3 : Form
    {
        System.Media.SoundPlayer soundPlayer = new System.Media.SoundPlayer(@"C:\Users\Olexandr\Desktop\Bankomat\zvuk11.wav");
        public Form3()
        {
            InitializeComponent();
            maskedTextBoxPIN.Mask = "0000";
            maskedTextBoxPIN.PasswordChar = '•'; // Встановлюємо символ приховування
            this.MaximizeBox = false;
            this.MinimizeBox = false;
        }
        string PIN;
        private void Form3_Load(object sender, EventArgs e)
        {

        }
        static bool CheckPinInDatabase(string connectionString, string pinToCheck)
        {
            using (SQLiteConnection connection = new SQLiteConnection(connectionString))
            {
                connection.Open();

                string selectQuery = "SELECT COUNT(*) FROM Cards WHERE PinCode = @PinCode";
                using (SQLiteCommand command = new SQLiteCommand(selectQuery, connection))
                {
                    command.Parameters.AddWithValue("@PinCode", pinToCheck);

                    int count = Convert.ToInt32(command.ExecuteScalar());

                    return count > 0;
                }
            }
        }
        private bool ValidatePinCode(string pinCode)
        {
            // логіка перевірки пін-коду
            string connectionString = "Data Source=database.db;Version=3;";
            bool pinExists = CheckPinInDatabase(connectionString, pinCode);

            if (pinExists)
            {
                return true;
            }
            else
            {
                return false;
            }
           
           
        }
        private void button10_Click(object sender, EventArgs e)
        {
            
        }

        private void button8_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
            string pinCode = maskedTextBoxPIN.Text;

            if (ValidatePinCode(pinCode))
            {
                Globals.Pin=pinCode;
                Form4 form4 = new Form4();
                form4.Show();
                this.Close();
            }
            else
            {
                Form7 form7 = new Form7();
                form7.Show();
                this.Close();
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }
    }
}
