﻿using Global;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static System.Windows.Forms.VisualStyles.VisualStyleElement;

namespace Main
{
    public partial class Form10 : Form
    {
        System.Media.SoundPlayer soundPlayer = new System.Media.SoundPlayer(@"C:\Users\Olexandr\Desktop\Bankomat\zvuk11.wav");

        public Form10()
        {
            InitializeComponent();
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            maskedTextBoxSuma.Validating += maskedTextBoxSuma_Validating;
        }
        private void maskedTextBoxSuma_Validating(object sender, CancelEventArgs e)
        {
            string enteredValue = maskedTextBoxSuma.Text;

            // Перевірка, чи введене значення є одним з дозволених
            if (enteredValue != "200" && enteredValue != "500" && enteredValue != "1000")
            {
                MessageBox.Show("Дозволені значення для суми: 200, 500 або 1000");
                e.Cancel = true; // Скасувати подію Validating, щоб запобігти втраті фокусу
            }
        }
        private void Form10_Load(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

            maskedTextBoxSuma.Text = string.Empty;
        }
        decimal Balance = 0;
       string CardNumber;
        string PinCode;
        private void button8_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

            string connectionString = "Data Source=database.db;Version=3;";
           
            using (SQLiteConnection connection = new SQLiteConnection(connectionString))
            {
                connection.Open();

                // Пошук по номеру телефону 
                string searchPhoneNumber = Globals.Phone;

                string selectQuery = "SELECT * FROM Cards WHERE PhoneNumber = @PhoneNumber";
                using (SQLiteCommand command = new SQLiteCommand(selectQuery, connection))
                {
                    command.Parameters.AddWithValue("@PhoneNumber", searchPhoneNumber);

                    using (SQLiteDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                            
                           CardNumber = reader.GetString(0);
                             PinCode = reader.GetString(1);
                            Balance = reader.GetDecimal(2);
                            

                            
                        }
                        else
                        {
                            Form7 form7 = new Form7();
                            form7.Show();
                            this.Close();
                        }
                    }
                }
            }
           
            string sum = maskedTextBoxSuma.Text;
            if (Balance < decimal.Parse(sum))
            {
                string filePath = "C:\\Users\\Olexandr\\Desktop\\Bankomat\\Number - TakeMoney.txt";
                try
                {
                    // Відкриваємо файл для запису
                    using (StreamWriter writer = new StreamWriter(filePath, true))
                    {
                        // Записуємо
                        writer.WriteLine($"{CardNumber}\t-\tПОМИЛКА");
                        Form7 form7 = new Form7();
                        form7.Show();
                        this.Close();

                    }


                }
                catch
                {

                }
               
            }
            else
            {

                string filePath = "C:\\Users\\Olexandr\\Desktop\\Bankomat\\Number - TakeMoney.txt";
                try
                {
                    // Відкриваємо файл для запису
                    using (StreamWriter writer = new StreamWriter(filePath, true))
                    {
                        // Записуємо
                        writer.WriteLine($"{CardNumber}\t-\t{sum} гривень");
                        Globals.Pin = PinCode;
                        Form11 form11 = new Form11();
                        form11.Show();
                        this.Close();
                    }


                }
                catch
                {

                }
                
            }
        }

        private void maskedTextBoxSuma_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
            
        }

        private void button6_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }
    }
}
