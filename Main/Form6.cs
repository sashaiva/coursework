﻿using Global;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Global;
namespace Main
{
    public partial class Form6 : Form
    {
        System.Media.SoundPlayer soundPlayer = new System.Media.SoundPlayer(@"C:\Users\Olexandr\Desktop\Bankomat\zvuk11.wav");

        public Form6()
        {
            InitializeComponent();
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            textBox1.Text=Globals.Number;
            textBox2.Text=Globals.Sum;
        }

        private void Form6_Load(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

            Form1 form1 = new Form1();
            form1.Show();
            this.Close();
        }
        decimal balance = 0;
        private void button8_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

            string connectionString = "Data Source=database.db;Version=3;";
            
            using (SQLiteConnection connection = new SQLiteConnection(connectionString))
            {
                connection.Open();

                // Пошук
                string searchPin = Globals.Pin;
               
                string selectQuery = "SELECT * FROM Cards WHERE PinCode = @PinCode";
                using (SQLiteCommand command = new SQLiteCommand(selectQuery, connection))
                {
                    command.Parameters.AddWithValue("@PinCode", searchPin);

                    using (SQLiteDataReader reader = command.ExecuteReader())
                    {
                        if (reader.Read())
                        {
                           
                            string retrievedCardNumber = reader.GetString(0);
                            string retrievedPinCode = reader.GetString(1);
                            decimal retrievedBalance = reader.GetDecimal(2);
                            balance = retrievedBalance;
                            
                        }
                        else
                        {
                            Form7 form7 = new Form7();
                            form7.Show();
                            this.Close();

                        }
                    }
                }
            }
            
            string number = textBox1.Text;
            string sum = textBox2.Text;
            if (balance<decimal.Parse(sum))
            {
                string filePath = "C:\\Users\\Olexandr\\Desktop\\Bankomat\\Number - Sum.txt";
                try
                {
                    // Відкриваємо файл для запису
                    using (StreamWriter writer = new StreamWriter(filePath, true))
                    {
                        // Записуємо
                        writer.WriteLine($"{number}\t-\tПОМИЛКА");
                    }


                }
                catch
                {

                }
                Form7 form7 = new Form7();
                form7.Show();
                this.Close();
            }
            else
            {
               
                string filePath = "C:\\Users\\Olexandr\\Desktop\\Bankomat\\Number - Sum.txt";
                try
                {
                    // Відкриваємо файл для запису
                    using (StreamWriter writer = new StreamWriter(filePath, true))
                    {
                        // Записуємо
                        writer.WriteLine($"{number}\t-\t{sum} гривень");
                    }

            
                }
                catch
                {

                }
                Form8 form8 = new Form8();
                form8.Show();
                this.Close();
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }
    }
}
