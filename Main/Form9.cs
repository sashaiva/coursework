﻿using Global;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Main
{
    public partial class Form9 : Form
    {
        System.Media.SoundPlayer soundPlayer = new System.Media.SoundPlayer(@"C:\Users\Olexandr\Desktop\Bankomat\zvuk11.wav");

        public Form9()
        {
            InitializeComponent();
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            maskedTextBoxNumber.Mask = "+380 (00) 000-00-00";
        }
        private bool IsUkrainianPhoneNumberValid(string phoneNumber)
        {

            string digitsOnly = new string(phoneNumber.Where(char.IsDigit).ToArray());


            if (digitsOnly.Length != 12)
            {
                return false;
            }


            if (digitsOnly[0] != '3' || digitsOnly[1] != '8' || digitsOnly[2] != '0')
            {
                return false;
            }

            if (!digitsOnly.Substring(3).All(char.IsDigit))
            {
                return false;
            }

            return true;
        }
        string Phone;
        private void button8_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

            if (maskedTextBoxNumber.MaskCompleted)
            {




                string phoneNumber = maskedTextBoxNumber.Text.Replace(" ", "").Replace("(", "").Replace(")", "").Replace("-", "");
                if (!IsUkrainianPhoneNumberValid(phoneNumber))
                {
                    MessageBox.Show("Неправильний формат номеру телефону. Введіть український номер телефону у форматі +380 (XX) XXX-XX-XX.", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    maskedTextBoxNumber.Focus();
                }
                else
                {
                    Phone = phoneNumber;
                   
                    Form10 form10 = new Form10();
                    form10.Show();
                    this.Close();
                   
                    Globals.Phone = Phone;
                }




            }
            else
            {
                MessageBox.Show("Будь ласка, введіть повний номер телефону.", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

            maskedTextBoxNumber.Text = string.Empty;
        }

        private void Form9_Load(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }
    }
}
