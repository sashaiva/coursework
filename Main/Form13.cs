﻿using Global;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Main
{
    public partial class Form13 : Form
    {
        System.Media.SoundPlayer soundPlayer = new System.Media.SoundPlayer(@"C:\Users\Olexandr\Desktop\Bankomat\zvuk11.wav");

        public Form13()
        {
            InitializeComponent();
            maskedTextBoxPIN.Mask = "0000";
            maskedTextBoxPIN.PasswordChar = '•'; // Встановлюємо символ приховування
            this.MaximizeBox = false;
            this.MinimizeBox = false;
        }
        static bool CheckPinInDatabase(string connectionString, string pinToCheck)
        {
            using (SQLiteConnection connection = new SQLiteConnection(connectionString))
            {
                connection.Open();

                string selectQuery = "SELECT COUNT(*) FROM Cards WHERE PinCode = @PinCode";
                using (SQLiteCommand command = new SQLiteCommand(selectQuery, connection))
                {
                    command.Parameters.AddWithValue("@PinCode", pinToCheck);

                    int count = Convert.ToInt32(command.ExecuteScalar());

                    return count > 0;
                }
            }
        }
        private bool ValidatePinCode(string pinCode)
        {
            // Виконайте логіку перевірки пін-коду
            string connectionString = "Data Source=database.db;Version=3;";
            bool pinExists = CheckPinInDatabase(connectionString, pinCode);

            if (pinExists)
            {
                return true;
            }
            else
            {
                return false;
            }


        }
        private void button8_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

            string pinCode = maskedTextBoxPIN.Text;

            if (ValidatePinCode(pinCode))
            {
                Globals.Pin = pinCode;
                Form14 form14 = new Form14();
                form14.Show();
                this.Close();
            }
            else
            {
                MessageBox.Show("Пін код не правильний", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void Form13_Load(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }

        private void button6_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }

        private void button5_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }
    }
}
