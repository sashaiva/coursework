﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Main
{
    public partial class Form8 : Form
    {
        System.Media.SoundPlayer soundPlayer = new System.Media.SoundPlayer(@"C:\Users\Olexandr\Desktop\Bankomat\zvuk11.wav");
        System.Media.SoundPlayer suc = new System.Media.SoundPlayer(@"C:\Users\Olexandr\Desktop\Bankomat\suc.wav");

        public Form8()
        {
            InitializeComponent();
            this.MaximizeBox = false;
            this.MinimizeBox = false;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

            Form1 form1 = new Form1();
            form1.Show();
            this.Close();
        }

        private void Form8_Load(object sender, EventArgs e)
        {
            suc.Play();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }
    }
}
