using Global;
using System.Data.SQLite;
using System.Media;
namespace Main
{
    public partial class Form1 : Form
    {
        System.Media.SoundPlayer soundPlayer = new System.Media.SoundPlayer(@"C:\Users\Olexandr\Desktop\Bankomat\zvuk11.wav");

        public Form1()
        {
            InitializeComponent();
          
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            string connectionString = "Data Source=database.db;Version=3;";

           
            using (SQLiteConnection connection = new SQLiteConnection(connectionString))
            {
                connection.Open();
              
                    string deleteQuery = "DELETE FROM Cards";
                    using (SQLiteCommand command = new SQLiteCommand(deleteQuery, connection))
                    {
                        command.ExecuteNonQuery();
                    }
                
               


               
                AddCardToDatabase(connectionString, "5375414143603511", "1234", 1000.0m, "+380963591473");
                AddCardToDatabase(connectionString, "5375467673603511", "4321", 500.0m, "+380978765600");
                AddCardToDatabase(connectionString, "5375490908013511", "1488", 2000.0m, "+3809318862233");
                AddCardToDatabase(connectionString, "5375490908011488", "1122", 9000.0m, "+380936126067");
                AddCardToDatabase(connectionString, "5375412312141211", "9999", 200.0m, "+380931886233");
                AddCardToDatabase(connectionString, "5375858585767451", "2754", 3000.0m, "+380973003413");
            }
        }

       

        static void AddCardToDatabase(string connectionString, string cardNumber, string pinCode, decimal balance, string phoneNumber)
        {
            using (SQLiteConnection connection = new SQLiteConnection(connectionString))
            {
                connection.Open();

                string insertQuery = "INSERT INTO Cards (CardNumber, PinCode, Balance, PhoneNumber) VALUES (@CardNumber, @PinCode, @Balance, @PhoneNumber)";
                using (SQLiteCommand command = new SQLiteCommand(insertQuery, connection))
                {
                    command.Parameters.AddWithValue("@CardNumber", cardNumber);
                    command.Parameters.AddWithValue("@PinCode", pinCode);
                    command.Parameters.AddWithValue("@Balance", balance);
                    command.Parameters.AddWithValue("@PhoneNumber", phoneNumber);

                    command.ExecuteNonQuery();
                }
            }
        }

       
        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void pictureBox2_Click(object sender, EventArgs e)
        {

        }
       
        private void button3_Click(object sender, EventArgs e)
        {
           
            soundPlayer.Play();

            Form2 form2 = new Form2();
            form2.Show();
            this.Hide(); // ��������� ��������� �����

        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
            Form9 form9 = new Form9();
            form9.Show();
            this.Hide(); 
        }

        private void button7_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
            Form13 form13 = new Form13();
            form13.Show();
            this.Hide(); 
        }

        private void button5_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }
    }
}