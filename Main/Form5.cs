﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Windows.Forms;
using Global;
namespace Main
{
    public partial class Form5 : Form
    {
        System.Media.SoundPlayer soundPlayer = new System.Media.SoundPlayer(@"C:\Users\Olexandr\Desktop\Bankomat\zvuk11.wav");

        public Form5()
        {
            InitializeComponent();
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            maskedTextBoxSuma.Text = "10000";
            textBox1.Text = "20";
        }

        private void Form5_Load(object sender, EventArgs e)
        {
           
        }
        double Sum;
        private void button7_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

            maskedTextBoxSuma.Text = string.Empty;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

            Form1 form1 = new Form1();
            form1.Show();
            this.Close();
        }

        private void maskedTextBoxSuma_MaskInputRejected(object sender, MaskInputRejectedEventArgs e)
        {
            maskedTextBoxSuma.TextChanged += maskedTextBoxSuma_TextChanged;
        }
        private void maskedTextBoxSuma_TextChanged(object sender, EventArgs e)
        {
            
            string sumaText = maskedTextBoxSuma.Text;
           
            if (sumaText!="")
            {
                
                double suma = double.Parse(sumaText);
                double komisiya = suma * 0.02;
                Sum = suma + komisiya;
                
                textBox1.Text = komisiya.ToString("F2");
               
            }
            else
            {
                textBox1.Text = "";
            }
            
        }
        
        private void button8_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

            string SUM =Sum.ToString();
            Globals.Sum = SUM;
            Form6 form6 = new Form6();
            form6.Show();
            this.Close();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }
    }
}
