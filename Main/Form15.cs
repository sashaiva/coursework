﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Global;
namespace Main
{
    public partial class Form15 : Form
    {
        System.Media.SoundPlayer soundPlayer = new System.Media.SoundPlayer(@"C:\Users\Olexandr\Desktop\Bankomat\zvuk11.wav");

        public Form15()
        {
            InitializeComponent();
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            textBox1.Text = Globals.Number;
            textBox2.Text = Globals.Sum;
            timer1.Tick += Timer_Tick;
        }
        private void Timer_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            Form1 form1 = new Form1();
            form1.Show();

            this.Close();
        }
        private void Form15_Load(object sender, EventArgs e)
        {
            timer1.Start();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }

        private void button7_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }

        private void button6_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }

        private void button5_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }
    }
}
