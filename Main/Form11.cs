﻿using Global;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Main
{
    public partial class Form11 : Form
    {
        System.Media.SoundPlayer soundPlayer = new System.Media.SoundPlayer(@"C:\Users\Olexandr\Desktop\Bankomat\zvuk11.wav");

        public Form11()
        {
            InitializeComponent();
            maskedTextBoxPIN.Mask = "0000";
            maskedTextBoxPIN.PasswordChar = '•'; // Встановлюємо символ приховування
            this.MaximizeBox = false;
            this.MinimizeBox = false;
        }

        private void Form11_Load(object sender, EventArgs e)
        {

        }
        string PIN = Globals.Pin;
        private bool ValidatePinCode(string pinCode)
        {
            //логіка перевірки пін-коду
           
           

            if (PIN==pinCode)
            {
                return true;
            }
            else
            {
                return false;
            }


        }
        private void button8_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

            string pinCode = maskedTextBoxPIN.Text;

            if (ValidatePinCode(pinCode))
            {
                Globals.Pin = pinCode;
                
                Form12 form12 = new Form12();
                form12.Show();
                this.Close();
                
            }
            else
            {
                MessageBox.Show("Пін код не правильний", "Помилка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }
    }
}
