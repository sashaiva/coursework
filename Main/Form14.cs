﻿using Global;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Main
{
    public partial class Form14 : Form
    {
        System.Media.SoundPlayer soundPlayer = new System.Media.SoundPlayer(@"C:\Users\Olexandr\Desktop\Bankomat\zvuk11.wav");

        public Form14()
        {
            InitializeComponent();
            this.MaximizeBox = false;
            this.MinimizeBox = false;
        }
        string Phone;
        
        decimal Balance = 0;
        string CardNumber;
        private void Form14_Load(object sender, EventArgs e)
        {
            string connectionString = "Data Source=database.db;Version=3;";

            using (SQLiteConnection connection = new SQLiteConnection(connectionString))
            {
                connection.Open();

                string query = "SELECT * FROM Cards WHERE PinCode = @PinCode";
                using (SQLiteCommand command = new SQLiteCommand(query, connection))
                {
                    command.Parameters.AddWithValue("@PinCode", Globals.Pin);

                    using (SQLiteDataReader reader = command.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            CardNumber = reader.GetString(0);

                            Balance = reader.GetDecimal(2);


                        }
                    }
                }

                Globals.Number = CardNumber;
                Globals.Sum = Balance.ToString();
            }
            }

        private void button4_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

            Form4 form4 = new Form4();
            form4.Show();
            this.Close();

        }

        private void button8_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();


            Form15 form15 = new Form15();
                form15.Show();
                this.Close();
            
        }

        private void button7_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();


            Form16 form16 = new Form16();
                form16.Show();
                this.Close();
            
        }

        private void button3_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

            Form17 form17 = new Form17();
            form17.Show();
            this.Close();
        }

        private void button6_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }

        private void button5_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }
    }
}

