﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Media;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Main
{
    public partial class Form12 : Form
    {
        System.Media.SoundPlayer soundPlayer = new System.Media.SoundPlayer(@"C:\Users\Olexandr\Desktop\Bankomat\zvuk11.wav");
        System.Media.SoundPlayer money = new System.Media.SoundPlayer(@"C:\Users\Olexandr\Desktop\Bankomat\money.wav");

        public Form12()
        {
            InitializeComponent();
            timer1.Tick += Timer_Tick;

        }
        private void Timer_Tick(object sender, EventArgs e)
        {
            timer1.Stop();
            Form1 form1 = new Form1();
            form1.Show();
           
            this.Close();
        }
        private void Form12_Load(object sender, EventArgs e)
        {
            timer1.Start();
            money.Play();
        }

        private void button8_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();
        }

        private void button7_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }

        private void button6_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }

        private void button5_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }

        private void button2_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }

        private void button1_Click(object sender, EventArgs e)
        {
            soundPlayer.Play();

        }
    }
}
